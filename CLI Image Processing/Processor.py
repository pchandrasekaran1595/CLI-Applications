import os
import cv2
import sys
import getpass
import numpy as np
from openvino import inference_engine

#########################################################################################################

def blur(image):
    ak, gk, gsigma, mk = None, None, None, None

    args = ["--average", "--gauss", "--median"]
    
    if args[0] in sys.argv:
        ak = int(sys.argv[sys.argv.index(args[0]) + 1])
    elif args[1] in sys.argv:
        gk, gsigma = int(sys.argv[sys.argv.index(args[1]) + 1]), \
                     int(sys.argv[sys.argv.index(args[1]) + 2])
    elif args[2] in sys.argv:
        mk = int(sys.argv[sys.argv.index(args[2]) + 1])
        if mk % 2 == 0:
            mk += 1
        if mk == 1:
            mk = 3
    
    if ak:
        image = cv2.blur(src=image, ksize=(ak, ak))
    if gk and gsigma:
        image = cv2.GaussianBlur(src=image, ksize=(gk, gk), sigmaX=gsigma)
    if mk:
        image = cv2.medianBlur(src=image, ksize=mk)
    
    return image
    
#########################################################################################################

def gamma_correct(image, gamma):
    image = image/255
    image = ((image ** gamma) * 255).astype("uint8")
    return image


def linear_contrast(image, alpha):
    return np.clip((image + alpha), 0, 255).astype("uint8")


def hist_equ(image):
    if len(image.shape) == 3:
        for i in range(3):
            image[:, :, i] = cv2.equalizeHist(image[:, :, i])
    else:
        image = cv2.equalizeHist(image)
    return image


def adaptive_equ(image, clipLimit, TGS):
    clahe = cv2.createCLAHE(clipLimit=clipLimit, tileGridSize=(TGS, TGS))
    if len(image.shape) == 3:
        for i in range(3):
            image[:, :, i] = clahe.apply(image[:, :, i])
    else:
        image = clahe.apply()
    return image


def contrast(image):
    gamma, linear, clipLimit, TGS, histequ = None, None, None, None, None

    args = ["--gamma", "--linear", "--clahe", "--histequ"]

    if args[0] in sys.argv:
        gamma = float(sys.argv[sys.argv.index(args[0]) + 1])
    elif args[1] in sys.argv:
        linear = float(sys.argv[sys.argv.index(args[1]) + 1])
    elif args[2] in sys.argv:
        clipLimit, TGS = float(sys.argv[sys.argv.index(args[2]) + 1]), int(sys.argv[sys.argv.index(args[2]) + 2])
    elif args[3] in sys.argv:
        histequ = True

    if gamma:
        image = gamma_correct(image, gamma)
    if linear:
        image = linear_contrast(image, linear)
    if clipLimit and TGS:
        image = adaptive_equ(image, clipLimit, TGS)
    if histequ:
        image = hist_equ(image)
    
    return image

#########################################################################################################

USER = getpass.getuser()
base_model_path = "C:/Users/" + USER + "/Documents/openvino_models/ir/public"
classes = [
    "background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair",
    "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
    "sofa", "train", "tvmonitor",
]


def get_model(model_name=None):
    ie = inference_engine.IECore()
    device = "MYRIAD" if "MYRIAD" in ie.available_devices else "CPU"

    model_path = os.path.join(os.path.join(base_model_path, model_name), "FP16")
    network = ie.read_network(model=os.path.join(model_path, model_name+".xml"), 
                              weights=os.path.join(model_path, model_name+".bin"))
    network = ie.load_network(network, device, num_requests=0)
    input_blob, output_blob = next(iter(network.input_info)), next(iter(network.outputs))
    N, C, H, W = network.input_info[input_blob].input_data.shape

    return network, (input_blob, output_blob), (N, C, H, W)


def detect(image):
    args = "--model-name"
    model_name = "ssd300"
    
    if args in sys.argv:
        model_name = sys.argv[sys.argv.index(args) + 1]

    network, (input_blob, output_blob), (N, C, H, W) = get_model(model_name=model_name)

    orig_image = image.copy()
    disp_image = image.copy()
    dh, dw, _ = disp_image.shape
    image = cv2.resize(src=image, dsize=(W, H), interpolation=cv2.INTER_AREA).transpose(2, 0, 1)
    result = network.infer({input_blob : image})[output_blob][0][0]

    best_index = np.argmax(result[:, 2])
    x1, y1, x2, y2 = int(result[best_index][3] * dw), int(result[best_index][4] * dh), \
                     int(result[best_index][5] * dw), int(result[best_index][5] * dh)
    cv2.rectangle(img=disp_image, pt1=(x1, y1), pt2=(x2, y2), color=(255, 0, 0), thickness=2)
    cv2.putText(img=disp_image, org=(x1-10, y1-10), text=classes[int(result[best_index][1])],
                fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(255, 0, 0), thickness=2)
    return orig_image, disp_image

#########################################################################################################

# Source: https://en.wikipedia.org/wiki/Floyd%E2%80%93Steinberg_dithering
def floyd_steinberg_dither(image, num_colors):
    image = image/255
    h, w, c = image.shape
    for c in range(c):
        for i in range(h-1):
            for j in range(1, w-1):
                old_pixel = image[i][j][c]
                new_pixel = find_closest_color(old_pixel, num_colors)
                image[i][j][c] = new_pixel
                quant_error = old_pixel - new_pixel

                image[i][j+1][c]   = image[i][j+1][c] + (quant_error * 7/16)
                image[i+1][j+1][c] = image[i+1][j+1][c] + (quant_error * 1/16)
                image[i+1][j][c]   = image[i+1][j][c] + (quant_error * 5/16)
                image[i+1][j-1][c] = image[i+1][j-1][c] + (quant_error * 3/16)
    return np.clip((image*255), 0, 255).astype("uint8")


def find_closest_color(pixel, num_colors):
    colors = [i*(1/num_colors) for i in range(num_colors+1)]
    distances = [abs(colors[i]-pixel) for i in range(len(colors))]
    index = distances.index(min(distances))
    return colors[index]


def dither(image):
    num_colors = 8 
    args = "--num-colors"
    
    if args in sys.argv:
        num_colors = int(sys.argv[sys.argv.index(args) + 1])
    
    return floyd_steinberg_dither(image, num_colors)

#########################################################################################################

def edge(image):
    threshold = 0.10

    args = "--thresh"

    if args in sys.argv:
        threshold = float(sys.argv[sys.argv.index(args) + 1])
    
    image = image/255
    image = np.sqrt((image[:, :-1] - image[:, 1:])**2)[1:, :] + np.sqrt((image[:-1, :] - image[1:, :])**2)[: , 1:]
    image[image > threshold] = 1
    image[image <= threshold] = 0
    image = (image * 255).astype("uint8")

    return threshold, image

#########################################################################################################

def adjust_hue(image, hue):
    feature = image[:, :, 0]
    feature = np.clip((hue * feature), 0, 179).astype("uint8")
    image[:, :, 0] = feature
    return cv2.cvtColor(src=image, code=cv2.COLOR_HSV2RGB)


def adjust_saturation(image, saturation):
    feature = image[:, :, 1]
    feature = np.clip((saturation * feature), 0, 255).astype("uint8")
    image[:, :, 1] = feature
    return cv2.cvtColor(src=image, code=cv2.COLOR_HSV2RGB)


def adjust_vibrance(image, vibrance):
    feature = image[:, :, 2]
    feature = np.clip((vibrance * feature), 0, 255).astype("uint8")
    image[:, :, 2] = feature
    return cv2.cvtColor(src=image, code=cv2.COLOR_HSV2RGB)


def hsv(image):
    hue, saturation, vibrance = None, None, None

    args = ["--hue", "--saturation", "--vibrance"]

    if args[0] in sys.argv:
        hue = float(sys.argv[sys.argv.index(args[0]) + 1])
    elif args[1] in sys.argv:
        saturation = float(sys.argv[sys.argv.index(args[1]) + 1])
    elif args[2] in sys.argv:
        vibrance = float(sys.argv[sys.argv.index(args[2]) + 1])
    
    image = cv2.cvtColor(src=image, code=cv2.COLOR_RGB2HSV)

    if hue:
        image = adjust_hue(image, hue)
    if saturation:
        image = adjust_saturation(image, saturation)
    if vibrance:
        image = adjust_vibrance(image, vibrance)
    
    return image

#########################################################################################################

def pixelate(image):
    h, w, _ = image.shape
    image = cv2.resize(src=image, dsize=(int(w/25), int(h/25)), interpolation=cv2.INTER_AREA)
    return cv2.resize(src=image, dsize=(w, h), interpolation=cv2.INTER_AREA)

#########################################################################################################

def do_posterize(image, num_colors):
    h, w, c = image.shape
    image = image/255
    for c in range(c):
        for i in range(h):
            for j in range(w):
                image[i][j][c] = new_color(image[i][j][c], num_colors)
    return np.clip((image*255), 0, 255).astype("uint8")


def new_color(pixel, num_colors):
    colors = [(1/num_colors)*i for i in range(num_colors)]
    distances = [abs(pixel-colors[i]) for i in range(len(colors))]
    index = distances.index(min(distances))
    return colors[index]


def posterize(image):
    num_colors = 8 
    args = "--num-colors"
    
    if args in sys.argv:
        num_colors = int(sys.argv[sys.argv.index(args) + 1])
    if num_colors == 1:
        num_colors = 2
    
    return do_posterize(image, num_colors)

#########################################################################################################

def resize(image):
    width = 1920
    height = 1080

    args_1 = ["--width", "-w"]
    args_2 = ["--height", "-h"]

    if args_1[0] in sys.argv:
        width = int(sys.argv[sys.argv.index(args_1[0]) + 1])
    elif args_1[1] in sys.argv:
        width = int(sys.argv[sys.argv.index(args_1[1]) + 1])

    if args_2[0] in sys.argv:
        height = int(sys.argv[sys.argv.index(args_2[0]) + 1])
    elif args_2[1] in sys.argv:
        height = int(sys.argv[sys.argv.index(args_2[1]) + 1])

    return cv2.resize(image, dsize=(width, height), interpolation=cv2.INTER_AREA)

#########################################################################################################

def decode(class_index_image=None):
    colors = np.array([(0, 0, 0), (128, 0, 0), (0, 128, 0), (128, 128, 0), (0, 0, 128), (128, 0, 128),
                       (0, 128, 128), (128, 128, 128), (64, 0, 0), (192, 0, 0), (64, 128, 0),
                       (192, 128, 0), (64, 0, 128), (192, 0, 128), (64, 128, 128), (192, 128, 128),
                       (0, 64, 0), (128, 64, 0), (0, 192, 0), (128, 192, 0), (0, 64, 128)])

    r, g, b = np.zeros(class_index_image.shape, dtype=np.uint8), \
              np.zeros(class_index_image.shape, dtype=np.uint8), \
              np.zeros(class_index_image.shape, dtype=np.uint8)

    for i in range(21):
        indexes = (class_index_image == i)
        r[indexes] = colors[i][0]
        g[indexes] = colors[i][1]
        b[indexes] = colors[i][2]
    return np.stack([r, g, b], axis=2)


def segment(image):
    args = "--model-name"
    model_name = "deeplabv3"
    
    if args in sys.argv:
        model_name = sys.argv[sys.argv.index(args) + 1]
    
    network, (input_blob, output_blob), (N, C, H, W) = get_model(model_name=model_name)
    
    im_h, im_w, _ = image.shape
    image = cv2.resize(src=image, dsize=(W, H), interpolation=cv2.INTER_AREA).transpose(2, 0, 1)
    class_index_image = network.infer({input_blob: image})[output_blob]

    # breaker()
    # print(" --- Classes Present ---\n")
    # class_index = np.unique(class_index_image)
    # for i in range(len(class_index)):
    #     print("{}. {}".format(i+1, classes[class_index[i]]))
    # breaker()

    return cv2.resize(src=decode(class_index_image[0]), dsize=(im_w, im_h), interpolation=cv2.INTER_AREA)

#########################################################################################################

def sharpen(image):
    k_size = 1
    args = "--k"

    if args in sys.argv:
        k_size = int(sys.argv[sys.argv.index(args) + 1])
        if k_size % 2 == 0:
            k_size = k_size + 1

    kernel = cv2.getStructuringElement(shape=cv2.MORPH_CROSS, ksize=(k_size, k_size)) * -1
    kernel[int(k_size / 2), int(k_size / 2)] = ((k_size - 1) * 2) + 1

    image = cv2.filter2D(src=image, ddepth=-1, kernel=kernel)
    return np.clip(image, 0, 255).astype("uint8")

#########################################################################################################
