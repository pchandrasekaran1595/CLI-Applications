import os
import cv2
import sys
import matplotlib.pyplot as plt

from Processor import blur, contrast, dither, detect, edge, hsv, pixelate, \
                      posterize, resize, segment, sharpen

def app():
    args_1 = "--blur"
    args_2 = "--contrast"
    args_3 = "--detect"
    args_4 = "--dither"
    args_5 = "--edge"
    args_6 = "--gray"
    args_7 = "--hsv"
    args_8 = "--posterize"
    args_9 = "--resize"
    args_10 = "--segment"
    args_11 = "--sharpen"
    args_12 = "--pixelate"
    args_13 = "--name"
    args_14 = "--save"

    SAVE_PATH = "./Processed"
    READ_PATH = "./Images"
    name = "car.jpg"
    save = None
    
    do_blur = None
    do_contrast = None
    do_detect = None
    do_dither = None
    do_edge = None
    do_gray = None
    do_hsv = None
    do_posterize = None
    do_resize = None
    do_segment = None
    do_sharpen = None
    do_pixelate = None

    if args_1 in sys.argv: do_blur = True
    if args_2 in sys.argv: do_contrast = True
    if args_3 in sys.argv: do_detect = True
    if args_4 in sys.argv: do_dither = True
    if args_5 in sys.argv: do_edge = True
    if args_6 in sys.argv: do_gray = True
    if args_7 in sys.argv: do_hsv = True
    if args_8 in sys.argv: do_posterize = True
    if args_9 in sys.argv: do_resize = True
    if args_10 in sys.argv: do_segment = True
    if args_11 in sys.argv: do_sharpen = True
    if args_12 in sys.argv: do_pixelate = True
    if args_13 in sys.argv: name = sys.argv[sys.argv.index(args_13) + 1]
    if args_14 in sys.argv: save = True

    image = cv2.cvtColor(src=cv2.imread(os.path.join(READ_PATH, name), cv2.IMREAD_COLOR), code=cv2.COLOR_BGR2RGB)

    if do_blur:
        image = blur(image)
    if do_contrast:
        image = contrast(image)
    if do_dither:
        image = dither(image)
    if do_detect:
        _, image = detect(image)
    if do_edge:
        image = cv2.cvtColor(src=image, code=cv2.COLOR_RGB2GRAY)
        _, image = edge(image)
        image = cv2.cvtColor(src=image, code=cv2.COLOR_GRAY2RGB)   
    if do_gray:
        image = cv2.cvtColor(src=image, code=cv2.COLOR_RGB2GRAY)  
    if do_hsv:
        image = hsv(image)
    if do_posterize:
        image = posterize(image)
    if do_resize:
        image = resize(image)
    if do_segment:
        image = segment(image)
    if do_sharpen:
        image = sharpen(image)
    if do_pixelate:
        image = pixelate(image)
    
    if save:
        cv2.imwrite(os.path.join(SAVE_PATH, "{}_Processed.jpg".format(name[:-4])), 
                    cv2.cvtColor(src=image, code=cv2.COLOR_BGR2RGB))
    else:
        plt.figure("Processed Image")
        if do_gray:
            plt.imshow(image, cmap="gray")
        else:
            plt.imshow(image)
        plt.axis("off")
        plt.show()
