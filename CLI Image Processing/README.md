Read Directory - "./Images"

Save Directory - "./Processed"

---
### **CLI ARGUMENTS**
---

1. `--name`      - Name of the Image (Including extension)

2. `--save`      - Flag used to control whether processed image is saved

3. `--blur`
    1. `--average` - Kernel Size of Average Blur Filter
    2. `--gauss`   - Kernel Size and SigmaX of Gaussian Blur Filter
    3. `--median`  - Kernel Size of Median Filter

4. `--contrast` 
    1. `--gamma`   - Gamma Contrast adjustment value
    2. `--linear`  - Linear Contrast adjustment value
    3. `--clahe`   - Cliplimit and TileGridSize for clahe operation
    4. `--histequ` - Flag to control histogram equalization

5. `--detect`
    1. `--model-name` - Name of the model used. Typically the folder can be found in /../openvino_models/public/ir

6. `--dither`
    1. `--num-colors` - Num of colors to use in the dither

7. `--edge`
    1. `--thresh` - Threshold used in edge detection (Default: 0.10)

8. `--gray` 

9. `--hsv`
    1. `--hue`        - Hue adjust value
    2. `--saturation` - Saturation adjust value
    3. `--vibrance`   - Vibrance adjust value

10. `--pixelate`

11. `--posterize` 
    1. `--num-colors` - Num of colors to use in the dither

12. `--resize`
    1. `--width` , `-w`  - Width of the image
    2. `--height` , `-h` - Height of the image

13. `--segment`
    1. `--model-name` - Name of the model used. Typically the folder can be found in /../openvino_models/public/ir
 
14. `--sharpen`
    1. `-k` - Kernel size of sharpening filter. Must be an odd number.
