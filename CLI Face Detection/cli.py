import os
import sys
import cv2
import torch
import platform
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from facenet_pytorch import MTCNN

PATH = "./Files"
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#####################################################################################################

def breaker(num=50, char="*"):
    print("\n" + num*char + "\n") 


def preprocess(image: np.ndarray) -> np.ndarray:
    return cv2.cvtColor(src=image, code=cv2.COLOR_BGR2RGB)


def downscale(image: np.ndarray, factor: int) -> np.ndarray:
    h, w, _ = image.shape
    return cv2.resize(src=image, dsize=(int(w/factor), int(h/factor)), interpolation=cv2.INTER_AREA)


def show(image: np.ndarray, title: str) -> None:
    plt.figure(figsize=(8, 6))
    plt.imshow(image)
    plt.axis("off")
    plt.title(title)
    plt.show()

#####################################################################################################

def downsample(image: np.ndarray, size: int) -> np.ndarray:
    return cv2.resize(src=image, dsize=(size, size), interpolation=cv2.INTER_AREA)


def single_face_detect(model, image, size):
    x1, y1, x2, y2 = None, None, None, None
    h, w, _ = image.shape
    pil_image = Image.fromarray(downsample(image, size))
    boxes, _ = model.detect(pil_image)

    if boxes is not None:
        index = 0 
        x1, y1, x2, y2 = int(boxes[index][0] * (w/size)), \
                         int(boxes[index][1] * (h/size)), \
                         int(boxes[index][2] * (w/size)), \
                         int(boxes[index][3] * (h/size))
    return x1, y1, x2, y2


def face_detect(model, image, size):
    x1, y1, x2, y2 = [], [], [], []
    h, w, _ = image.shape
    pil_image = Image.fromarray(downsample(image, size))
    boxes, _ = model.detect(pil_image)

    if boxes is not None:
        for box in boxes:
            x1.append(int(box[0] * (w/size))), y1.append(int(box[1] * (h/size))), x2.append(int(box[2] * (w/size))), y2.append(int(box[3] * (h/size)))
    return x1, y1, x2, y2

#####################################################################################################

def draw_single_box(image: np.ndarray, x1: int, y1: int, x2: int, y2: int, thickness: int) -> None:
    cv2.rectangle(img=image, pt1=(x1, y1), pt2=(x2, y2), color=(0, 255, 0), thickness=thickness)


def draw_all_boxes(image: np.ndarray, x1: list, y1: list, x2: list, y2: list, thickness: int) -> np.ndarray:
    for i in range(len(x1)):
        cv2.rectangle(img=image, pt1=(x1[i], y1[i]), pt2=(x2[i], y2[i]), color=(0, 255, 0), thickness=thickness)

#####################################################################################################

def app():
    args_1 = "--image"
    args_2 = "--video"
    args_3 = "--realtime"
    args_4 = "--all"
    args_5 = "--name"
    args_6 = "--size"
    args_7 = "--downscale"

    do_image, do_video, do_realtime, do_downscale, do_all = None, None, None, None, None
    name = None
    size = 160

    if args_1 in sys.argv:
        do_image = True
    if args_2 in sys.argv:
        do_video = True
    if args_3 in sys.argv:
        do_realtime = True
    if args_4 in sys.argv:
        do_all = True
    if args_5 in sys.argv:
        name = sys.argv[sys.argv.index(args_5) + 1]
    if args_6 in sys.argv:
        size = int(sys.argv[sys.argv.index(args_6) + 1])
    if args_7 in sys.argv:
        do_downscale = True
        factor = float(sys.argv[sys.argv.index(args_7) + 1])
    
    model = MTCNN(image_size=size, keep_all=True, device=DEVICE)

    if do_image:
        assert(name is not None)
        image = preprocess(cv2.imread(os.path.join(PATH, name), cv2.IMREAD_COLOR))
        assert(image is not None)
        disp_image = image.copy()

        if do_all:
            x1, y1, x2, y2 = face_detect(model, image, size)
            if len(x1) == 0:
                breaker()
                print("No Faces Detected")
                breaker()
            else:
                draw_all_boxes(disp_image, x1, y1, x2, y2, 3)
                show(disp_image, "Detected")
        else:
            x1, y1, x2, y2 = single_face_detect(model, image, size)
            if x1 is None:
                breaker()
                print("No Faces Detected")
                breaker()
            else:
                draw_single_box(disp_image, x1, y1, x2, y2, 3)
                show(disp_image, "Detected")

    if do_video:
        cap = cv2.VideoCapture(os.path.join(PATH, name))

        while cap.isOpened():
            ret, frame = cap.read()

            if ret:
                if do_downscale:
                    frame = downscale(frame, factor)
                
                disp_frame = frame.copy()
                frame = preprocess(frame)

                if do_all:
                    x1, y1, x2, y2 = face_detect(model, frame, size)
                    if len(x1) == 0:
                        pass
                    else:
                        draw_all_boxes(disp_frame, x1, y1, x2, y2, 2)
                else:
                    x1, y1, x2, y2 = single_face_detect(model, frame, size)
                    if x1 is None:
                        pass
                    else:
                        draw_single_box(disp_frame, x1, y1, x2, y2, 2)

                cv2.imshow("Face Detector", disp_frame)
                if cv2.waitKey(1) == ord("q"):
                    break
            else:
                cap.set(cv2.CAP_PROP_FRAME_COUNT, 0)
        
        cap.release()
        cv2.destroyAllWindows()

    if do_realtime:
        if platform.system() != "Windows":
            cap = cv2.VideoCapture(0)
        else:            
            cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 360)
        cap.set(cv2.CAP_PROP_FPS, 30)

        while cap.isOpened():
            _, frame = cap.read()
            disp_frame = frame.copy()
            frame = preprocess(frame)

            if do_all:
                x1, y1, x2, y2 = face_detect(model, frame, size)
                if len(x1) == 0:
                    pass
                else:
                    draw_all_boxes(disp_frame, x1, y1, x2, y2, 2)
            else:
                x1, y1, x2, y2 = single_face_detect(model, frame, size)
                if x1 is None:
                    pass
                else:
                    draw_single_box(disp_frame, x1, y1, x2, y2, 2)

            cv2.imshow("Face Detector", disp_frame)
            if cv2.waitKey(1) == ord("q"):
                break
        
        cap.release()
        cv2.destroyAllWindows()

#####################################################################################################
