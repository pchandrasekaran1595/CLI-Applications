Face Detection CLI Application using facenet-pytorch.

Assumes Pytorch is already installed. To get it to work without having to install pytorch, remove import statement from `cli.py` and change,

`DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")` to `DEVICE = "cpu"`

### **CLI Arguments:**
<pre>
1. --image     : Flag that controls entry to perform detection on an image
2. --video     : Flag that controls entry to perform detection on a video file
3. --realtime  : Flag that controls entry to perform realtime detection
4. --all       : Flag that controls whether to detect all faces in the image/video
5. --name      : Name of the file (Used when --image or --video is set)
6. --size      : Image size to be used by the model
7. --downscale : Used to downscale the video file (Useful for display purposes)
</pre>

Needs --image, --video or --realtime

&nbsp;

**Notes**:

Add pyinstaller command to build a .exe