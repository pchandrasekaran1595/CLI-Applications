## CLI-Applications

1. CLI Image Processing   - Image Processing Operations from the command line

2. CLI Video Processing   - Image Processing Operations done on Video from the command line

3. CLI OpenVINO Inference - Neural Network Inference using the OpenVINO Library performed from the command line. 

4. CLI Torchvision        - Inference on the various computer vision models provided by the pytorch library.

5. CLI Face Detection     - Face Detection using facenet-pytorch.
