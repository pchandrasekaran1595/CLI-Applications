import os
import cv2
import sys
import numpy as np
from imgaug import augmenters

#########################################################################################################

def blur(image):
    gk, gsigma = 3, 0
    args = "--gauss"
    
    if args in sys.argv:
        gk, gsigma = int(sys.argv[sys.argv.index(args) + 1]), \
                     int(sys.argv[sys.argv.index(args) + 2])

    image = cv2.GaussianBlur(src=image, ksize=(gk, gk), sigmaX=gsigma)
    return image

#########################################################################################################

def gamma_correct(image, gamma):
    image = image/255
    image = ((image ** gamma) * 255).astype("uint8")
    return image


def adaptive_equ(image, clipLimit, TGS):
    clahe = cv2.createCLAHE(clipLimit=clipLimit, tileGridSize=(TGS, TGS))
    if len(image.shape) == 3:
        for i in range(3):
            image[:, :, i] = clahe.apply(image[:, :, i])
    else:
        image = clahe.apply()
    return image


def linear_contrast(image, alpha):
    return np.clip((image + alpha), 0, 255).astype("uint8")


def hist_equ(image):
    if len(image.shape) == 3:
        for i in range(3):
            image[:, :, i] = cv2.equalizeHist(image[:, :, i])
    else:
        image = cv2.equalizeHist(image)
    return image


def contrast(image):
    gamma, linear, clipLimit, TGS, histequ = None, None, None, None, None

    args = ["--gamma", "--linear", "--clahe", "--histequ"]

    if args[0] in sys.argv:
        gamma = float(sys.argv[sys.argv.index(args[0]) + 1])
    elif args[1] in sys.argv:
        linear = float(sys.argv[sys.argv.index(args[1]) + 1])
    elif args[2] in sys.argv:
        clipLimit, TGS = float(sys.argv[sys.argv.index(args[2]) + 1]), int(sys.argv[sys.argv.index(args[2]) + 2])
    elif args[3] in sys.argv:
        histequ = True

    if gamma:
        image = gamma_correct(image, gamma)
    if linear:
        image = linear_contrast(image, linear)
    if clipLimit and TGS:
        image = adaptive_equ(image, clipLimit, TGS)
    if histequ:
        image = hist_equ(image)
    
    return image

#########################################################################################################

def edge(image):
    threshold = 0.10
    args = "--thresh"

    if args in sys.argv:
        threshold = float(sys.argv[sys.argv.index(args) + 1])
    
    image = image/255
    image = np.sqrt((image[:, :-1] - image[:, 1:])**2)[1:, :] + np.sqrt((image[:-1, :] - image[1:, :])**2)[: , 1:]
    image[image > threshold] = 1
    image[image <= threshold] = 0
    image = (image * 255).astype("uint8")

    return threshold, image

#########################################################################################################

def adjust_hue(image, hue):
    feature = image[:, :, 0]
    feature = np.clip((hue * feature), 0, 179).astype("uint8")
    image[:, :, 0] = feature
    return cv2.cvtColor(src=image, code=cv2.COLOR_HSV2RGB)


def adjust_saturation(image, saturation):
    feature = image[:, :, 1]
    feature = np.clip((saturation * feature), 0, 255).astype("uint8")
    image[:, :, 1] = feature
    return cv2.cvtColor(src=image, code=cv2.COLOR_HSV2RGB)


def adjust_vibrance(image, vibrance):
    feature = image[:, :, 2]
    feature = np.clip((vibrance * feature), 0, 255).astype("uint8")
    image[:, :, 2] = feature
    return cv2.cvtColor(src=image, code=cv2.COLOR_HSV2RGB)


def hsv(image):
    hue, saturation, vibrance = None, None, None

    args = ["--hue", "--saturation", "--vibrance"]

    if args[0] in sys.argv:
        hue = float(sys.argv[sys.argv.index(args[0]) + 1])
    elif args[1] in sys.argv:
        saturation = float(sys.argv[sys.argv.index(args[1]) + 1])
    elif args[2] in sys.argv:
        vibrance = float(sys.argv[sys.argv.index(args[2]) + 1])
    image = cv2.cvtColor(src=image, code=cv2.COLOR_RGB2HSV)
    
    if hue:
        image = adjust_hue(image, hue)
    if saturation:
        image = adjust_saturation(image, saturation)
    if vibrance:
        image = adjust_vibrance(image, vibrance)

    return image

#########################################################################################################

def sharpen(image):
    k_size = 1
    args = "--k"

    if args in sys.argv:
        k_size = int(sys.argv[sys.argv.index(args) + 1])
        if k_size % 2 == 0:
            k_size = k_size + 1

    kernel = cv2.getStructuringElement(shape=cv2.MORPH_CROSS, ksize=(k_size, k_size)) * -1
    kernel[int(k_size / 2), int(k_size / 2)] = ((k_size - 1) * 2) + 1

    image = cv2.filter2D(src=image, ddepth=-1, kernel=kernel)
    image = np.clip(image, 0, 255).astype("uint8")
    return image

#########################################################################################################

def mask(image):
    threshold = 0.10
    args = "--thresh"

    if args in sys.argv:
        threshold = float(sys.argv[sys.argv.index(args) + 1])

    n_image = image.copy()
    image = cv2.cvtColor(src=image, code=cv2.COLOR_RGB2GRAY)
    _, tframe = cv2.threshold(src=image, thresh=threshold, maxval=255, type=cv2.THRESH_BINARY)

    for i in range(3):
        n_image[:, :, i] = tframe & n_image[:, :, i]
    
    return n_image

#########################################################################################################

def pixelate(image):
    h, w, _ = image.shape
    image = cv2.resize(src=image, dsize=(int(w/25), int(h/25)), interpolation=cv2.INTER_AREA)
    return cv2.resize(src=image, dsize=(w, h), interpolation=cv2.INTER_AREA)

#########################################################################################################
