import sys
import cli


def main():
    args = "--combine"
    if args in sys.argv:
        cli.app_2()
    else:
        cli.app_1()


if __name__ == '__main__':
    sys.exit(main() or 0)
