import os
import sys
import cv2

from Processor import blur, contrast, edge, hsv, mask, pixelate, sharpen
from Combiner import combine, combine_alpha

def app_1():
    args_1 = "--blur"
    args_2 = "--contrast"
    args_3 = "--edge"
    args_4 = "--gray"
    args_5 = "--hsv"
    args_6 = "--sharpen"
    args_7 = "--mask"
    args_8 = "--pixelate"
    args_9 = "--name"
    args_10 = "--save"
    args_11 = "--width"
    args_12 = "--height"
    args_13 = "--speed"

    SAVE_PATH = "./Processed"
    READ_PATH = "./Files"
    name = "FILE_NAME.mp4"
    save = None
    speed = 1

    do_blur = None
    do_contrast = None
    do_edge = None
    do_gray = None
    do_hsv = None
    do_sharpen = None
    do_mask = None
    do_pixelate = None

    if args_1 in sys.argv: do_blur = True
    if args_2 in sys.argv: do_contrast = True
    if args_3 in sys.argv: do_edge = True
    if args_4 in sys.argv: do_gray = True
    if args_5 in sys.argv: do_hsv = True
    if args_6 in sys.argv: do_sharpen = True
    if args_7 in sys.argv: do_mask = True
    if args_8 in sys.argv: do_pixelate = True
    if args_9 in sys.argv: name = sys.argv[sys.argv.index(args_9) + 1]
    if args_10 in sys.argv: save = True
    
    cap = cv2.VideoCapture(os.path.join(READ_PATH, name))
    w, h = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    if args_11 in sys.argv: w = int(sys.argv[sys.argv.index(args_11) + 1])
    if args_12 in sys.argv: h = int(sys.argv[sys.argv.index(args_12) + 1])
    if args_13 in sys.argv: speed = int(sys.argv[sys.argv.index(args_13) + 1])

    if save:
        out = cv2.VideoWriter(os.path.join(SAVE_PATH, "Processed - {}.mp4".format(name[:-4])), cv2.VideoWriter_fourcc(*"mp4v"), 30, (w, h))

    while cap.isOpened():
        ret, frame = cap.read()
        if ret:
            frame = cv2.resize(src=frame, dsize=(w, h), interpolation=cv2.INTER_AREA)
            if do_blur:
                frame = blur(frame)
            if do_contrast:
                frame = contrast(frame)
            if do_edge:
                _, frame = edge(frame)
            if do_gray:
                frame = cv2.cvtColor(src=frame, code=cv2.COLOR_BGR2GRAY)
                frame = cv2.cvtColor(src=frame, code=cv2.COLOR_GRAY2BGR)
            if do_hsv:
                frame = hsv(frame)
            if do_sharpen:
                frame = sharpen(frame)
            if do_mask:
                frame = mask(frame)
            if do_pixelate:
                frame = pixelate(frame)
            if save:
                out.write(frame)
                if cv2.waitKey(1) == ord("q"):
                    break
            else:
                cv2.imshow("{}".format(name[:-4]), frame)
                if cv2.waitKey(speed) == ord("q"):
                    break
        else:
            if save:
                break
            else:
                cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

    cap.release()
    cv2.destroyAllWindows()

#########################################################################################################
         
def app_2():
    args_1_0 = "--name1"
    args_1_1 = "--name2"
    args_2 = "--alpha"
    args_3 = "--combinevid"
    args_4 = "--combineimg"
    args_5 = "--alphavid"
    args_6 = "--alphaimg"
    args_7 = "--save"
    args_8 = "--width"
    args_9 = "--height"
    
    do_combine_vid = None
    do_combine_img = None
    do_alpha_vid = None
    do_alpha_img = None

    alpha = 0.1
    SAVE_PATH = "./Processed"
    READ_PATH = "./Files"
    name_1, name_2 = None, None
    save = None

    if args_1_0 in sys.argv: name_1 = sys.argv[sys.argv.index(args_1_0) + 1]
    if args_1_1 in sys.argv: name_2 = sys.argv[sys.argv.index(args_1_1) + 1]
    if args_2 in sys.argv: alpha = float(sys.argv[sys.argv.index(args_2) + 1])
    if args_3 in sys.argv: do_combine_vid = True
    if args_4 in sys.argv: do_combine_img = True
    if args_5 in sys.argv: do_alpha_vid = True
    if args_6 in sys.argv: do_alpha_img = True
    if args_7 in sys.argv: save = True

    cap_1 = cv2.VideoCapture(os.path.join(READ_PATH, name_1))
    if do_combine_vid or do_alpha_vid:
        cap_2 = cv2.VideoCapture(os.path.join(READ_PATH, name_2))
    if do_combine_img or do_alpha_img:
        image = cv2.imread(os.path.join(READ_PATH, name_2))
        # image = image[:, ::-1]
        # image = image[::-1, :]
    
    w, h = int(cap_1.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap_1.get(cv2.CAP_PROP_FRAME_HEIGHT))
    if args_8 in sys.argv: w = int(sys.argv[sys.argv.index(args_8) + 1])
    if args_9 in sys.argv: h = int(sys.argv[sys.argv.index(args_9) + 1])

    if save:
        if do_combine_vid or do_combine_img:
            out = cv2.VideoWriter(os.path.join(SAVE_PATH, "Combined.mp4"), cv2.VideoWriter_fourcc(*"mp4v"), 30, (2*w, h))
        else:
            out = cv2.VideoWriter(os.path.join(SAVE_PATH, "Combined.mp4"), cv2.VideoWriter_fourcc(*"mp4v"), 30, (w, h))
    
    if do_combine_vid:
        while cap_1.isOpened() and cap_2.isOpened():
            ret_1, frame_1 = cap_1.read()
            ret_2, frame_2 = cap_2.read()
            if ret_1 and ret_2:
                frame_1 = cv2.resize(src=frame_1, dsize=(w, h), interpolation=cv2.INTER_AREA)
                frame_2 = cv2.resize(src=frame_2, dsize=(w, h), interpolation=cv2.INTER_AREA)
                frame = combine(frame_1, frame_2)
                if save:
                    out.write(frame)
                else:
                    cv2.imshow("Combined", frame)
                if cv2.waitKey(1) == ord("q"):
                    break
            else:
                if save:
                    break
                else:
                    cap_1.set(cv2.CAP_PROP_POS_FRAMES, 0)
                    cap_2.set(cv2.CAP_PROP_POS_FRAMES, 0)
        cap_1.release()
        cap_2.release()
        if save:
            out.release()
    
    if do_combine_img:
        while cap_1.isOpened():
            ret_1, frame_1 = cap_1.read()
            if ret_1:
                frame_1 = cv2.resize(src=frame_1, dsize=(w, h), interpolation=cv2.INTER_AREA)
                image = cv2.resize(src=image, dsize=(w, h), interpolation=cv2.INTER_AREA)
                frame = combine(frame_1, image)
                if save:
                    out.write(frame)
                else:
                    cv2.imshow("Combined", frame)
                if cv2.waitKey(1) == ord("q"):
                    break
            else:
                if save:
                    break
                else:
                    cap_1.set(cv2.CAP_PROP_POS_FRAMES, 0)
        cap_1.release()
        if save:
            out.release()
    
    if do_alpha_vid:
        while cap_1.isOpened() and cap_2.isOpened():
            ret_1, frame_1 = cap_1.read()
            ret_2, frame_2 = cap_2.read()
            if ret_1 and ret_2:
                frame_1 = cv2.resize(src=frame_1, dsize=(w, h), interpolation=cv2.INTER_AREA)
                frame_2 = cv2.resize(src=frame_2, dsize=(w, h), interpolation=cv2.INTER_AREA)
                frame = combine_alpha(frame_1, frame_2, alpha)
                if save:
                    out.write(frame)
                else:
                    cv2.imshow("Combined", frame)
                if cv2.waitKey(1) == ord("q"):
                    break
            else:
                if save:
                    break
                else:
                    cap_1.set(cv2.CAP_PROP_POS_FRAMES, 0)
                    cap_2.set(cv2.CAP_PROP_POS_FRAMES, 0)
        cap_1.release()
        cap_2.release()
        if save:
            out.release()
    
    if do_alpha_img:
        while cap_1.isOpened():
            ret_1, frame_1 = cap_1.read()
            if ret_1:
                frame_1 = cv2.resize(src=frame_1, dsize=(w, h), interpolation=cv2.INTER_AREA)
                image = cv2.resize(src=image, dsize=(w, h), interpolation=cv2.INTER_AREA)
                frame = combine_alpha(frame_1, image, alpha)
                if save:
                    out.write(frame)
                else:
                    cv2.imshow("Combined", frame)
                if cv2.waitKey(1) == ord("q"):
                    break
            else:
                if save:
                    break
                else:
                    cap_1.set(cv2.CAP_PROP_POS_FRAMES, 0)
        cap_1.release()
        if save:
            out.release()

#########################################################################################################
