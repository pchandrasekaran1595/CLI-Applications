
Input Folder = "./Files"

Save Folder  = "./Processed"

---
### **CLI ARGUMENTS**
---
1. 
    1. `--name` - Name of the Video File (Including extension)
    2. `--name1`, `--name2` - Name of the Video File (Including extension) used with `--combine`

2.  `--save` - Flag used to control whether processed video is saved. If not set, then displays video in a cv2 window.

3. 
    1. `--width` - Width of the processed Video
    2. `--height` - Height of the processed Video
    Note: Both arguments, if used, are applied before processing.

4. `--blur`
    1. `--gauss`   - Kernel Size and SigmaX of Gaussian Blur Filter

5. `--contrast` 
    1. `--gamma`   - Gamma Contrast adjustment value
    2. `--linear`  - Linear Contrast adjustment value
    3. `--clahe`   - Cliplimit and TileGridSize for clahe operation
    4. `--histequ` - Flag to control histogram equalization

6. `--edge`
    1. `--thresh` - Threshold used in edge detection (Default: 0.10)

7. `--gray` 

8. `--hsv`
    1. `--hue`        - Hue adjust value
    2. `--saturation` - Saturation adjust value
    3. `--vibrance`   - Vibrance adjust value
 
9. `--sharpen`
    1. `-k` - Kernel size of sharpening filter. Must be an odd number.
