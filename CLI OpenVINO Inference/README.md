# CLI-Arguments

Default Model Directory: "PATH_TO_MAIN.PY/Models"

Default Image Directory: "PATH_TO_MAIN.PY/Images"

1. `--classify`   - Flag to perform classification

2. `--detect`     - Flag to perform detection

3. `--segment`    - Flag to perform segmentation

4. `--realtime`   - Flag to perform realtime inference (Default: False ==> Image Inference)

5. `--filename`   - Name of the image file (If needed)

6. `--model-name` - Name of the model used
    1. Default Classification: mobilenet-v3-small-1.0-224-tf
    2. Default Detection     : ssd_mobilenet_v2_coco
    3. Default Segmentation  : deeplabv3
 