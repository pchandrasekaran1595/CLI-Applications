import os
import cv2
import sys
import platform
import matplotlib.pyplot as plt
from openvino import inference_engine

import utils as u
from Handlers import classify, detect, segment

########################################################################################################

def get_model(model_name):
    ie = inference_engine.IECore()
    device = "MYRIAD" if "MYRIAD" in ie.available_devices else "CPU"

    nw_path = os.path.join(os.path.join(u.BASE_MODELS_PATH, model_name), u.PRECISION[1])
    network = ie.read_network(model=os.path.join(nw_path, model_name+".xml"), weights=os.path.join(nw_path, model_name+".bin"))
    network = ie.load_network(network, device, num_requests=0)
    
    input_blob, output_blob = next(iter(network.input_info)), next(iter(network.outputs))
    N, C, H, W = network.input_info[input_blob].input_data.shape

    return network, (input_blob, output_blob), (N, C, H, W)

########################################################################################################

def app():
    args_1 = "--classify"
    args_2 = "--detect"
    args_3 = "--segment"
    args_4 = "--realtime"
    args_5 = "--filename"
    args_6 = "--model-name"
    
    do_classify, do_detect, do_segment, do_realtime = None, None, None, None
    c_name, d_name, s_name, name = "mobilenet-v3-small-1.0-224-tf", "ssd_mobilenet_v2_coco", "deeplabv3", "car.jpg"

    if args_1 in sys.argv:
        do_classify = True
    if args_2 in sys.argv:
        do_detect = True
    if args_3 in sys.argv:
        do_segment = True
    if args_4 in sys.argv:
        do_realtime = True
    if args_5 in sys.argv:
        name = sys.argv[sys.argv.index(args_5) + 1]
    if args_6 in sys.argv:
        if do_classify:
            c_name = sys.argv[sys.argv.index(args_6) + 1]  
        if do_detect:
            d_name = sys.argv[sys.argv.index(args_6) + 1] 
        if do_segment:
            s_name = sys.argv[sys.argv.index(args_6) + 1]
            
    if do_classify:
        model, blobs, shape = get_model(model_name=c_name)
    if do_detect:
        model, blobs, shape = get_model(model_name=d_name)
    if do_segment:
        model, blobs, shape = get_model(model_name=s_name)

    if do_classify:
        if do_realtime:
            if platform.system() != "Windows":
                cap = cv2.VideoCapture(u.ID)
            else:
                cap = cv2.VideoCapture(u.ID, cv2.CAP_DSHOW)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, u.CAM_WIDTH)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, u.CAM_HEIGHT)
            cap.set(cv2.CAP_PROP_FPS, u.FPS)

            while cap.isOpened():
                _, frame = cap.read()

                frame, label_index = classify(frame, model, blobs, shape)
                cv2.putText(img=frame, org=(25, 75), text=u.simple_imagenet_labels[label_index],
                            fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0), thickness=2)
                cv2.imshow("Classified", frame)

                if cv2.waitKey(1) == ord("q"):
                    break
            cap.release()
            cv2.destroyAllWindows()
        else:
            image = cv2.cvtColor(src=cv2.imread(os.path.join(u.IMAGE_PATH, name)), code=cv2.COLOR_BGR2RGB)
            image, label_index = classify(image, model, blobs, shape)
            cv2.putText(img=image, org=(25, 75), text=u.simple_imagenet_labels[label_index],
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0), thickness=2)
            
            plt.figure()
            plt.imshow(image)
            plt.axis("off")
            plt.show()
    
    if do_detect:
        if do_realtime:
            if platform.system() != "Windows":
                cap = cv2.VideoCapture(u.ID)
            else:
                cap = cv2.VideoCapture(u.ID, cv2.CAP_DSHOW)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, u.CAM_WIDTH)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, u.CAM_HEIGHT)
            cap.set(cv2.CAP_PROP_FPS, u.FPS)

            while cap.isOpened():
                _, frame = cap.read()

                frame, pt1, pt2, label_index = detect(frame, model, blobs, shape)
                if label_index == 0:
                    pass
                else:
                    if d_name != "ssd300":
                        cv2.rectangle(img=frame, pt1=pt1, pt2=pt2, color=(0, 255, 0), thickness=4)
                        cv2.putText(img=frame, org=(int((pt1[0]+pt2[0])/2), int((pt1[1]+pt2[1])/2)), text=u.coco_labels[label_index],
                            fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0), thickness=2)
                    else:
                        cv2.rectangle(img=frame, pt1=pt1, pt2=pt2, color=(0, 255, 0), thickness=4)
                        cv2.putText(img=frame, org=(int((pt1[0]+pt2[0])/2), int((pt1[1]+pt2[1])/2)), text=u.pvoc_labels[label_index],
                                fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0), thickness=2)
                
                cv2.imshow("Detected", frame)

                if cv2.waitKey(1) == ord("q"):
                    break
            cap.release()
            cv2.destroyAllWindows()
        else:
            image = cv2.cvtColor(src=cv2.imread(os.path.join(u.IMAGE_PATH, name)), code=cv2.COLOR_BGR2RGB)
            image, pt1, pt2, label_index = detect(image, model, blobs, shape)
            if d_name != "ssd300":
                cv2.rectangle(img=image, pt1=pt1, pt2=pt2, color=(0, 255, 0), thickness=4)
                cv2.putText(img=image, org=(int((pt1[0]+pt2[0])/2), int((pt1[1]+pt2[1])/2)), text=u.coco_labels[label_index],
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0), thickness=2)
            else:
                cv2.rectangle(img=image, pt1=pt1, pt2=pt2, color=(0, 255, 0), thickness=4)
                cv2.putText(img=image, org=(int((pt1[0]+pt2[0])/2), int((pt1[1]+pt2[1])/2)), text=u.pvoc_labels[label_index],
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 255, 0), thickness=2)

            
            plt.figure()
            plt.imshow(image)
            plt.axis("off")
            plt.show()
    
    if do_segment:
        if do_realtime:
            if platform.system() != "Windows":
                cap = cv2.VideoCapture(u.ID)
            else:
                cap = cv2.VideoCapture(u.ID, cv2.CAP_DSHOW)
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, u.CAM_WIDTH)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, u.CAM_HEIGHT)
            cap.set(cv2.CAP_PROP_FPS, u.FPS)

            while cap.isOpened():
                _, frame = cap.read()

                frame, _= segment(frame, model, blobs, shape)
                cv2.imshow("Segmented", frame)

                if cv2.waitKey(1) == ord("q"):
                    break
            cap.release()
            cv2.destroyAllWindows()
        else:
            image = cv2.cvtColor(src=cv2.imread(os.path.join(u.IMAGE_PATH, name)), code=cv2.COLOR_BGR2RGB)
            image, _ = segment(image, model, blobs, shape)
            
            plt.figure()
            plt.imshow(image)
            plt.axis("off")
            plt.show()
    
########################################################################################################
        