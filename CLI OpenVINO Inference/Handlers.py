import cv2
import numpy as np
import utils as u

"""
    Note: Handlers may need to be modified for optimal working with differnet models.
"""

########################################################################################################

def decode(class_index_image=None):
    colors = np.array([(0, 0, 0), (128, 0, 0), (0, 128, 0), (128, 128, 0), (0, 0, 128), (128, 0, 128),
                       (0, 128, 128), (128, 128, 128), (64, 0, 0), (192, 0, 0), (64, 128, 0),
                       (192, 128, 0), (64, 0, 128), (192, 0, 128), (64, 128, 128), (192, 128, 128),
                       (0, 64, 0), (128, 64, 0), (0, 192, 0), (128, 192, 0), (0, 64, 128)])

    r, g, b = np.zeros(class_index_image.shape, dtype=np.uint8), \
              np.zeros(class_index_image.shape, dtype=np.uint8), \
              np.zeros(class_index_image.shape, dtype=np.uint8)

    for i in range(21):
        indexes = (class_index_image == i)
        r[indexes] = colors[i][0]
        g[indexes] = colors[i][1]
        b[indexes] = colors[i][2]
    return np.stack([r, g, b], axis=2)

########################################################################################################

def segment(image, model, blobs, shape):
    h, w, _ = image.shape
    disp_image = image.copy()

    image = cv2.cvtColor(src=image, code=cv2.COLOR_RGB2BGR)
    image = cv2.resize(src=image, dsize=(shape[2], shape[3]), interpolation=cv2.INTER_AREA).transpose(2, 0, 1)

    result = model.infer({blobs[0]: image})[blobs[1]][0]
    disp_image = cv2.resize(src=decode(result), dsize=(w, h), interpolation=cv2.INTER_AREA)
    return disp_image, np.unique(disp_image)

########################################################################################################

def detect(image, model, blobs, shape):
    h, w, _ = image.shape
    _, _, H, W = shape
    disp_image = image.copy()

    image = cv2.cvtColor(src=image, code=cv2.COLOR_RGB2BGR)
    image = cv2.resize(src=image, dsize=(shape[2], shape[3]), interpolation=cv2.INTER_AREA).transpose(2, 0, 1)

    result = model.infer({blobs[0]: image})[blobs[1]][0][0]
    best_box = np.argmax(result[:, 2])

    x1, y1, x2, y2 = int(result[best_box][3] * w), int(result[best_box][4] * h), int(result[best_box][5] * w), int(result[best_box][6] * h)
    return disp_image, (x1, y1), (x2, y2), int(result[best_box][1])

########################################################################################################

def classify(image, model, blobs, shape):
    disp_image = image.copy()

    image = cv2.cvtColor(src=image, code=cv2.COLOR_RGB2BGR)
    image = cv2.resize(src=image, dsize=(shape[2], shape[3]), interpolation=cv2.INTER_AREA).transpose(2, 0, 1)

    result = model.infer({blobs[0]: image})[blobs[1]]

    return disp_image, np.argmax(result)

########################################################################################################

